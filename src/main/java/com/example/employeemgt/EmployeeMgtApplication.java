package com.example.employeemgt;

import com.example.employeemgt.model.CEO;
import com.example.employeemgt.model.Department;
import com.example.employeemgt.model.Employee;
import com.example.employeemgt.model.Team;
import com.example.employeemgt.repository.CeoRepository;
import com.example.employeemgt.repository.DepartmentRepository;
import com.example.employeemgt.repository.EmployeeRepository;
import com.example.employeemgt.repository.TeamRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class EmployeeMgtApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeMgtApplication.class, args);
    }

    @Bean
    public CommandLineRunner mappingDemo(CeoRepository ceoRepository,
                                         DepartmentRepository departmentRepository, TeamRepository teamRepository, EmployeeRepository employeeRepository) {
        return args -> {

            CEO ceo = new CEO("I'm CEO");

            ceoRepository.save(ceo);
            Department department1 = new Department("Department 1",ceo);
            Department department2 = new Department("Department 2",ceo);
            Department department3 = new Department("Department 3",ceo);
            Department department4 = new Department("Department 4",ceo);
            Department department5 = new Department("Department 5",ceo);
            Department department6 = new Department("Department 6",ceo);
            Department department7 = new Department("Department 7",ceo);
            Department department8 = new Department("Department 8",ceo);
            departmentRepository.saveAll(Arrays.asList(department1,department2,department3,department4));

            Team team1 = new Team("Team 1",department1);
            Team team2 = new Team("Team 2",department1);
            Team team3 = new Team("Team 3",department2);
            Team team4 = new Team("Team 4",department2);
            teamRepository.saveAll(Arrays.asList(team1,team2,team3,team4));

            Employee employee1 = new Employee("Employee 1",23);
            Employee employee2 = new Employee("Employee 2",23);
            Employee employee3 = new Employee("Employee 3",23);
            Employee employee4 = new Employee("Employee 4",23);
            employeeRepository.saveAll(Arrays.asList(employee1,employee2,employee3,employee4));
            employee1.getTeams().addAll(Arrays.asList(team1,team2));
            employee2.getTeams().addAll(Arrays.asList(team3,team4));
            employeeRepository.saveAll(Arrays.asList(employee1,employee2));
//            team1.getEmployees().addAll(Arrays.asList(employee1,employee2));
//            team2.getEmployees().addAll(Arrays.asList(employee3,employee4));
//            teamRepository.saveAll(Arrays.asList(team1,team2));

        };
    }

}
