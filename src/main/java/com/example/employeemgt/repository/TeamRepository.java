package com.example.employeemgt.repository;

import com.example.employeemgt.model.Team;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeamRepository extends CrudRepository<Team, Long> {
    List<Team> findByNameContaining(String name);
}
