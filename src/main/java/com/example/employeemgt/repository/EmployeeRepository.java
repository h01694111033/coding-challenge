package com.example.employeemgt.repository;

import com.example.employeemgt.model.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee,Long> {
    List<Employee> findByNameContaining(String name);
    List<Employee> findFirst1500By();
}
