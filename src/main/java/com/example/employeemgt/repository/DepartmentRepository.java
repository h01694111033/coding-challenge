package com.example.employeemgt.repository;

import com.example.employeemgt.model.CEO;
import com.example.employeemgt.model.Department;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DepartmentRepository extends CrudRepository<Department,Long> {
    List<Department> findByCeo(CEO ceo, Sort sort);
}
