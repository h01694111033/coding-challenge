package com.example.employeemgt.repository;

import com.example.employeemgt.model.CEO;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CeoRepository extends CrudRepository<CEO,Long> {
    List<CEO> findByNameContaining(String name);
}
